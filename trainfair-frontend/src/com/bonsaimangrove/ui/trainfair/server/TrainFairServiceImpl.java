package com.bonsaimangrove.ui.trainfair.server;

import com.bonsaimangrove.ui.trainfair.client.TrainFairService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class TrainFairServiceImpl extends RemoteServiceServlet implements TrainFairService {

	private static final long serialVersionUID = 1L;

	@Override
	public String[] getStationNames() {
		String[] stations = new String[] { "Oxford", "London Paddington", "Bristol Parkway", "Norbiton" };
		return stations;
	}

}
