/**
 * 
 */
package com.bonsaimangrove.ui.trainfair.client;

import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.client.presenter.JourneyPresenter;
import com.bonsaimangrove.ui.trainfair.client.presenter.TimetablePresenter;
import com.bonsaimangrove.ui.trainfair.client.view.JourneyView;
import com.bonsaimangrove.ui.trainfair.client.view.TimetableViewImpl;
import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * @author matt
 * 
 */
public class AppController implements ValueChangeHandler<String> {
	private final HandlerManager eventBus;
	private HasWidgets container;
	private JourneyView journeyView = null;
	private JourneyPresenter journeyPresenter = null;
	private TimetableViewImpl timetableView = null;
	private TrainFairServiceAsync rpcService = null;

	public AppController(HandlerManager eventBus, TrainFairServiceAsync rpcService) {
		this.eventBus = eventBus;
		this.rpcService  = rpcService;
		bind();
	}

	private void bind() {
		History.addValueChangeHandler(this);
	}

	public void go(final HasWidgets container) {
		Logger.logger.log(Level.INFO, "AppController.go");
		this.container = container;

		// TODO Auto-generated method stub
		if ("".equals(History.getToken())) {
			History.newItem(HistoryToken.JOURNEY);
		} else {
			History.fireCurrentHistoryState();
		}

	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		String token = event.getValue();
		Logger.logger.log(Level.INFO, "AppController.onValueChange: " + token);
		if (token != null) {
			if (token.equals(HistoryToken.JOURNEY)) {
				GWT.runAsync(new RunAsyncCallback() {
					@Override
					public void onSuccess() {
						// lazily initialize our views, and keep them around to
						// be reused
						//
						if (journeyView == null) {
							journeyView = new JourneyView();
						}
						if (journeyPresenter == null) {
							journeyPresenter = new JourneyPresenter(eventBus, rpcService, journeyView);
						}
						// The example from which this was copied created a new Presenter each time,
						// but that loses state in the Presenter that we don't want to lose.
						journeyPresenter.go(container);
					}

					@Override
					public void onFailure(Throwable reason) {
						// TODO Auto-generated method stub

					}

				});
			}
			if (token.equals(HistoryToken.TIMETABLE)) {
				GWT.runAsync(new RunAsyncCallback() {
					@Override
					public void onSuccess() {
						// lazily initialize our views, and keep them around to
						// be reused
						//
						if (timetableView == null) {
							timetableView = new TimetableViewImpl();
						}
						new TimetablePresenter(eventBus, timetableView)
								.go(container);
					}

					@Override
					public void onFailure(Throwable reason) {
						// TODO Auto-generated method stub

					}

				});
			}
		}

	}

}
