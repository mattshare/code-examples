package com.bonsaimangrove.ui.trainfair.client.presenter;

import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.client.view.TimetableView;
import com.bonsaimangrove.ui.trainfair.client.view.TimetableView.Presenter;
import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;

public class TimetablePresenter implements Presenter {

	private final HandlerManager eventBus;
	private final TimetableView view;

	public TimetablePresenter(HandlerManager eventBus, TimetableView view) {
		this.eventBus = eventBus;
		this.view = view;

	}

	public void go(final HasWidgets container) {
		Logger.logger.log(Level.INFO, "TimetablePresenter.go");
		container.clear();
		container.add(view.asWidget());
	}

}
