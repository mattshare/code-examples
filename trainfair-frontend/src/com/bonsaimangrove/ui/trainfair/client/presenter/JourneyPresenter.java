package com.bonsaimangrove.ui.trainfair.client.presenter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.client.TrainFairServiceAsync;
import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class JourneyPresenter implements Presenter {
	private final HandlerManager eventBus;
	Display view;
	private TrainFairServiceAsync rpcService;

	public interface Display {
		public Widget asWidget();

		public void setPresenter(JourneyPresenter pres);

		public void setOutwardDateTime(Date d);

		public void setReturnDateTime(Date d);

		public void setStationNames(String[] result);
	}

	public JourneyPresenter(HandlerManager eventBus, TrainFairServiceAsync rpcService, Display view) {
		this.eventBus = eventBus;
		this.rpcService = rpcService;
		this.view = view;
		view.setOutwardDateTime(new Date());
		view.setReturnDateTime(new Date());
		doStationNames();
		bind();
	}

	private void doStationNames() {
		// Set up the callback object.
		AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {
			public void onFailure(Throwable caught) {
				// TODO: Do something with errors.
				Logger.logger.log(Level.WARNING, "getStations RPC call failed");
			}

			public void onSuccess(String[] result) {
				view.setStationNames(result);
			}
		};

		// Make the call to the stock price service.
		rpcService.getStationNames(callback);
	}

	@Override
	public void go(final HasWidgets container) {
		Logger.logger.log(Level.INFO, "JourneyPresenter.go");
		container.clear();
		container.add(view.asWidget());
	}

	@Override
	public void bind() {
		// TODO Auto-generated method stub
		view.setPresenter(this);
	}

	// We want to avoid the keep the outward and return date and time in step
	// with each other automatically, until the user sets them.
	private boolean rtnDateTimeSetByUser = false;
	private boolean outDateTimeSetByUser = false;

	public void onOutwardDateTimeChanged(Date d) {
		Logger.logger.log(Level.INFO, "onOutwardDateTimeChanged " + d.toString());
		outDateTimeSetByUser = true;
		if (!rtnDateTimeSetByUser) {
			view.setReturnDateTime(d);
		}
	}

	public void onReturnDateTimeChanged(Date d) {
		Logger.logger.log(Level.INFO, "onReturnDateTimeChanged " + d.toString());
		rtnDateTimeSetByUser = true;
		if (!outDateTimeSetByUser) {
			view.setOutwardDateTime(d);
		}
	}

}