package com.bonsaimangrove.ui.trainfair.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

public interface Presenter {
	public void bind();
	public void go(HasWidgets container);
}
