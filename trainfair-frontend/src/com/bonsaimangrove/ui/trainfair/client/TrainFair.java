package com.bonsaimangrove.ui.trainfair.client;

import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TrainFair implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		Logger.logger.log(Level.INFO, "Log: onModuleLoad");
		TrainFairServiceAsync rpcService = GWT.create(TrainFairService.class);
		HandlerManager eventBus = new HandlerManager(null);
		AppController appViewer = new AppController(eventBus, rpcService);
		// <div id="trainfairRootPanel"/> is defined in TrainFair.html:
		appViewer.go(RootPanel.get("trainfairRootPanel"));
	}

}
