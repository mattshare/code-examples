package com.bonsaimangrove.ui.trainfair.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TrainFairServiceAsync {

	void getStationNames(AsyncCallback<String[]> callback);

}
