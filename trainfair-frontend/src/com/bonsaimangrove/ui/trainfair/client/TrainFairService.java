package com.bonsaimangrove.ui.trainfair.client;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("trainfairService")
public interface TrainFairService extends RemoteService {

	String[] getStationNames();
}
