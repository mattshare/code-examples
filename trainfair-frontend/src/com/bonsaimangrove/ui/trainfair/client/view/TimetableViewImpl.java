package com.bonsaimangrove.ui.trainfair.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class TimetableViewImpl extends Composite implements TimetableView {
	@UiTemplate("TimetableView.ui.xml")
	interface TimetableViewUiBinder extends UiBinder<Widget, TimetableViewImpl> {
	}

	private static TimetableViewUiBinder uiBinder = GWT
			.create(TimetableViewUiBinder.class);

	private Presenter presenter;

	public TimetableViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

}