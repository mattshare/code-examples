package com.bonsaimangrove.ui.trainfair.client.view;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.client.HistoryToken;
import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.bonsaimangrove.ui.trainfair.client.presenter.JourneyPresenter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.Widget;

public class JourneyView extends Composite implements JourneyPresenter.Display {
	@UiTemplate("JourneyView.ui.xml")
	interface JourneyViewUiBinder extends UiBinder<Widget, JourneyView> {
	}

	private static JourneyViewUiBinder uiBinder = GWT
			.create(JourneyViewUiBinder.class);

	@UiField
	SuggestBox from;
	@UiField
	JourneyDateTime out;
	@UiField
	JourneyDateTime rtn;
	@UiField
	RadioButton sglButton;
	@UiField
	RadioButton rtnButton;
	@UiField
	Button getTimetable;

	private JourneyPresenter presenter = null;

	public JourneyView() {
		initWidget(uiBinder.createAndBindUi(this));
		rtn.setVisible(rtnButton.getValue());
	}

	public void setPresenter(JourneyPresenter presenter) {
		this.presenter = presenter;
	}

	@UiHandler("getTimetable")
	void getTimetableClicked(ClickEvent ev) {
		// AppController will handle the History ValueChangeEvent caused by ...
		History.newItem(HistoryToken.TIMETABLE);
	}

	@UiHandler("sglButton")
	void sglClicked(ClickEvent ev) {
		rtn.setVisible(false);
	}

	@UiHandler("rtnButton")
	void rtnClicked(ClickEvent ev) {
		rtn.setVisible(true);
	}

	public void setOutwardDateTime(Date d) {
		out.setValue(d);
	}

	public void setReturnDateTime(Date d) {
		rtn.setValue(d);
	}

	@UiHandler("out")
	void onOutwardDateTimeChanged(ValueChangeEvent<Date> event) {
		if (presenter != null) {
			presenter.onOutwardDateTimeChanged(event.getValue());
		}
	}

	@UiHandler("rtn")
	void onReturnDateTimeChanged(ValueChangeEvent<Date> event) {
		if (presenter != null) {
			presenter.onReturnDateTimeChanged(event.getValue());
		}
	}

	@Override
	public void setStationNames(String[] stations) {
		// TODO Auto-generated method stub
		MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) from.getSuggestOracle();
		orcl.addAll(Arrays.asList(stations));
	}
}