/**
 * 
 */
package com.bonsaimangrove.ui.trainfair.client.view;

import com.google.gwt.user.client.ui.Widget;

/**
 * @author matt
 * 
 */
public interface TimetableView {
	public interface Presenter {
//		void onOutDateChanged();
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

}
