package com.bonsaimangrove.ui.trainfair.client.view;

//import java.util.Calendar;
import java.util.Date;
//import java.util.GregorianCalendar;
import java.util.logging.Level;

import com.bonsaimangrove.ui.trainfair.shared.Logger;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class JourneyDateTime extends Composite implements
		HasValueChangeHandlers<Date>, HasValue<Date> {

	private static JourneyDateTimeUiBinder uiBinder = GWT
			.create(JourneyDateTimeUiBinder.class);

	interface JourneyDateTimeUiBinder extends UiBinder<Widget, JourneyDateTime> {
	}

	@UiField
	DateBox date;
	@UiField
	ListBox hour;
	@UiField
	ListBox min;

	@UiHandler("date")
	void handleDateChange(ValueChangeEvent<Date> event) {
		Date d = event.getValue();
		Logger.logger.log(Level.INFO, d.toString());
		fireValueChangeEvent();
	}

	@UiHandler("hour")
	void handleHourChange(ChangeEvent event) {
		Logger.logger.log(Level.INFO, hour.getValue(hour.getSelectedIndex()));
		fireValueChangeEvent();
	}

	@UiHandler("min")
	void handleMinChange(ChangeEvent event) {
		Logger.logger.log(Level.INFO, min.getValue(min.getSelectedIndex()));
		fireValueChangeEvent();
	}

	public JourneyDateTime() {
		initWidget(uiBinder.createAndBindUi(this));
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		date.setFormat(new DateBox.DefaultFormat(dateFormat));
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Date> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	private void fireValueChangeEvent() {
		ValueChangeEvent.fire(this, getValue());
	}

	@Override
	public Date getValue() {
		Date d = date.getValue();
		String h = hour.getValue(hour.getSelectedIndex());
		String m = min.getValue(min.getSelectedIndex());
//		Forced to use deprecated methods, because GWT does not support java.util.Calendar:
		d.setHours(Integer.parseInt(h));
		d.setMinutes(Integer.parseInt(m));
		d.setSeconds(0);
		return d;
//		Can't use Calendar in GWT:
//		Calendar cal = GregorianCalendar.getInstance();
//		cal.setTime(d);
//		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
//				cal.get(Calendar.DAY_OF_MONTH), Integer.parseInt(h),
//				Integer.parseInt(m));
//		ValueChangeEvent.fire(this, cal.getTime());
	}

	@Override
	public void setValue(Date value) {
		date.setValue(value);
		hour.setItemSelected(value.getHours(), true);
		min.setItemSelected(value.getMinutes()/15, true);
	}

	@Override
	public void setValue(Date value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
}
