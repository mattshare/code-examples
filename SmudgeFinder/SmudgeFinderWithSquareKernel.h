#ifndef SMUDGEFINDERWITHQUAREKERNEL
#define SMUDGEFINDERWITHQUAREKERNEL
#include <cassert>
#include <iostream>
#include <range/v3/all.hpp>
#include "SmudgeFinder.h"
#include "Image.h"
#include "Rect.h"

// This is an implementation of the SmudgeFinder.

namespace SmudgeFinder {

	// For Square Kernel with side N 
	// (no general implementation is provided for arbitrary N):
	template <int N> class SmudgeFinderWithSquareKernel;

	template <>
	// Specialization for 6x6 kernel.
	// The specialization is used here in order to make the code simpler:
	// We are providing an implementation only for squares with side 6,
	// so there's no point providing a more general implementation when
	// that creates complexity (e.g. handling squares with odd numbered sides).
	//
	// The specialization also has the benefit that any attempt to use 
	// a square with side other than 6 will simply fail to compile.

	class SmudgeFinderWithSquareKernel<6> : public SmudgeFinder {

		static const int SIDE = 6;

		// This algorithm considers all of the 6x6 squares of pixels contained in the image.
		// It looks for a circular smudge that is centred within the square.
		//
		// It considers the 'cross sections' through the square:
		// Each cross section is a either a 6x1 pixel row, or a 1x6 pixel column.
		//
		// Due to the rotational symmetry of looking for a circle centred in a square
		// (i.e. the same algorithm can be applied if the square is rotated by 90, 180, or 270 degrees),
		// it is useful to consider three classes of cross section:
		// (columns labelled 1, 2; rows labelled 3, 4)
		//
		//     Three classes of cross section
		//     ------------------------------
		//
		//         Central               Edges              In between
		//
		//           1 2              1         2            1     2
		//           ↓ ↓              ↓         ↓            ↓     ↓
		//      ┌───────────┐        ┌───────────┐        ┌───────────┐
		//      │- - X X - -│    3 → │X X X X X X│        │- X - - X -│
		//      │- - X X - -│        │X - - - - X│    3 → │X X X X X X│
		//  3 → │X X X X X X│        │X - - - - X│        │- X - - X -│
		//  4 → │X X X X X X│        │X - - - - X│        │- X - - X -│
		//      │- - X X - -│        │X - - - - X│    4 → │X X X X X X│
		//      │- - X X - -│    4 → │X X X X X X│        │- X - - X -│
		//      └───────────┘        └───────────┘        └───────────┘
		//
		// Each class contains 4 cross sections (2 rows, 2 columns), and can be identified
		// by the distance of its cross sections from the nearest edge:
		static const int EDGES_EDGE_DIST = 0;
		static const int INBETWEEN_EDGE_DIST = 1;
		static const int CENTRAL_EDGE_DIST = 2;

		// The function xSectsSatisfy will perform the same checks on all 4 cross sections
		// within the same class. The class is specified by the distance from the edge:

		template<typename Pred>
		bool xSectsSatisfy(	        // Returns true iff all specified cross sections satisfy the predicate.
				int col, int row,	// 'origin' of the 6x6 square within the image
				int distFromEdge,	// so 0 means all 4 edges, and 2 means the 4 'central' rows/columns
				Pred pred           // Predicate to be satisfied.
				) const {

			// i and j are offsets from 0 of the two 'opposite' (symmetric) cross sections.
			// (i, j) is either (0, 5) or (1, 4), or (2, 3).
			const int i = distFromEdge;
			const int j = SIDE - distFromEdge - 1;

			return
				// Check pred on each of the two 'opposite' rows:
				pred(image_.subRow(col, row + i, SIDE)) && 
				pred(image_.subRow(col, row + j, SIDE)) && 

				// Check pred on each of the two 'opposite' columns:
				pred(image_.subColumn(col + i, row, SIDE)) && 
				pred(image_.subColumn(col + j, row, SIDE)) 
				;
		}

		// The algorithm takes one parameter for 'tuning':
		// The minimum difference between the value of a central pixel (one of the 4
		// pixels in the middle of the 6x6 square), and that of its closest edge pixel:
		const int minCentralHeight_;

		// The function isSmudge is where each 6x6 square is checked:
		bool isSmudge(
				int col, int row	// defines the 'origin' of the 6x6 square to be checked.
				) const {

			// Run tests on the 'central' cross sections of the current square:
			bool res = xSectsSatisfy(col, row, CENTRAL_EDGE_DIST,
					[&](auto v) {	// v is a cross section of the square

					// We expect each cross section of the 6x6 square to contain 6 pixels:
					assert(v.size() == SIDE);
				return
					// Ensure the centre pixels are sufficiently raised:
					(v[2] - v[0] >= minCentralHeight_) && (v[3] - v[5] >= minCentralHeight_)
					
					// Ensure the peak is central:
					&& (v[3] >= v[1]) && (v[2] >= v[4])
					;
				});

			// TODO - check if there are any constraints on the square of 4 pixels at each corner
			//        of the 6x6 square.
			//        The above algorithm doesn't look at these, so it is more likely to find
			//        smudges that are close together (with their bounding rects intersecting),
			//        but this may not be what is required.

			return res;
		}

		// Sometimes the same smudge can be identified in two adjacent 6x6 squares.
		// We want to choose the square with the best fit.
		int fitnessMetric(int col, int row) {
			// Take the sum of the differences between each centre pixel
			// and its nearest edge pixel. 
			// Do this in both  directions (rows and columns) - that's why there's
			// a multiplication by 2.

			return
				// Central pixels:
				  2*(image_.pixel(col + 2, row + 2))
				+ 2*(image_.pixel(col + 2, row + 3))
				+ 2*(image_.pixel(col + 3, row + 2))
				+ 2*(image_.pixel(col + 3, row + 3))

				// Edge pixels:
				- image_.pixel(col + 0, row + 2)
				- image_.pixel(col + 0, row + 3)
				- image_.pixel(col + 5, row + 2)
				- image_.pixel(col + 5, row + 3)
				- image_.pixel(col + 2, row + 0)
				- image_.pixel(col + 3, row + 0)
				- image_.pixel(col + 2, row + 5)
				- image_.pixel(col + 3, row + 5);
		}

	public:
		explicit SmudgeFinderWithSquareKernel(const Image& image, int minCentralHeight = 2)
			: SmudgeFinder(image), minCentralHeight_(minCentralHeight)
			{
			}

		std::vector<Rect> findSmudges() {
			std::vector< std::pair<int, int> > smudges;	// store (col, row) as a pair

			// We iterate through all of the 6x6 square kernels within the image:
			for (int r = 0; r <= image_.height() - SIDE; ++r) {
				for (int c = 0; c <= image_.width() - SIDE; ++c) {
					if (isSmudge(c, r)) {
						// We'll be adding this smudge to the list of smudges that we've found,
						// unless there's 
						bool isSmudgeCloseToAnotherOne = false;
						// Look for nearby smudges already found:
						smudges
							// Go through found list in reverse ...
							| ranges::view::reverse

							// ... only as far back as rows that are close ...
							| ranges::view::take_while([&](auto& x) { return x.second >= r-3; })

							// ... and we're interested only in columns that are close ...
							| ranges::view::filter([&](auto& x) { return abs(x.first - c) <= 3; })

							// ... and change the existing smudge to the new one, if it's a better fit:
							| ranges::action::transform([&](auto x){
									// Need to flag the fact that the new smudge is being dealt with here:
									isSmudgeCloseToAnotherOne = true;

									return fitnessMetric(c, r) > fitnessMetric(x.first, x.second)
										? std::make_pair(c, r) // new smudge is a better fit.
										: x;                   // leave existing smudge in place.
								});
						if (!isSmudgeCloseToAnotherOne) {
							smudges.push_back(std::make_pair(c, r));
						}
					}
				}
			}
			std::vector<Rect> boundingRects;
			std::transform(smudges.begin(), smudges.end(), std::back_inserter(boundingRects),
					[](auto& x){ return Rect(x.first, x.second, SIDE, SIDE); });
			return boundingRects;
		}
	};

} // namespace SmudgeFinder
#endif // SMUDGEFINDERWITHQUAREKERNEL

