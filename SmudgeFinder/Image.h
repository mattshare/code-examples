#ifndef TIFFIMAGE_H
#define TIFFIMAGE_H
#include <iostream>
#include <vector>
#include <cstdint>  // for uint8_t
#include <range/v3/all.hpp>
#include <tiffio.h> // for libtiff: http://www.libtiff.org/
#include "Rect.h"

namespace SmudgeFinder {

	class Image {
		typedef uint8_t Pixel_t; // how pixel values are stored.
	public:
		Image(const std::string& name, TIFF* tif);

		// Return the pixel value at (col, row):
		Pixel_t pixel(int col, int row) const {
			return pixels_[row*width_ + col];
		}

		// Image dimensions:
		int width() const { return width_; }
		int height() const { return height_; }

		// The name of the image is of the form 
		//    f[i]
		// where f is the filename, and i is the number of the image within the TIFF file.
		const std::string& name() const { return name_; }

		// Return a V3 Range object for the sub-row of the image that
		// starts at (col, row) and contains exactly len pixels:
		decltype(auto) subRow(int col, int row, int len) const {
			assert(row >= 0);
			assert(col >= 0);
			assert(col + len <= width_);
			assert(row < height_);
			return pixels_ 
				| ranges::view::drop(row*width_ + col)
				| ranges::view::take(len);
		}

		// Return a V3 Range object for the sub-column of the image that
		// starts at (col, row) and contains exactly len pixels:
		decltype(auto) subColumn(int col, int row, int len) const {
			assert(row >= 0);
			assert(col >= 0);
			assert(row + len <= height_);
			assert(col < width_);
			return pixels_ 
				| ranges::view::drop(row*width_ + col)
				| ranges::view::stride(width_)
				| ranges::view::take(len);
		}

		// dump whole image to ostream:
		void dump(std::ostream& s) const;

		// dump part of image to ostream:
		void dump(std::ostream& s, const Rect& rect) const;

		// histogram returns a vector giving the frequency of the pixel
		// value i in the ith element of the vector.
		// This may be useful for calibrating algorithms.
		// (In fact it is not used in the current implementation).
		std::vector<int> histogram() const;
	private:
		const std::string name_;
		int width_;
		int height_;
		std::vector<Pixel_t> pixels_;

	};
} // namespace SmudgeFinder
#endif // TIFFIMAGE_H
