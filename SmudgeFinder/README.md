# Finding "Smudge"s in images

This program is an answer by Matt Wallis to the coding challenge set by Oxford Nano Imaging.

## Building

The program uses Ranges V3 from C++20. To use this on Ubuntu, I needed to install it:
```
sudo apt-get install librange-v3-dev
```

To build the program:
```
clang++ -O3 -DNDEBUG -std=c++17 -Wall -Wextra -pedantic *.cpp -ltiff -o find-smudges
```

## Running

The program takes a list of TIFF files:
```
./find-smudges <TIFF FILES>
```

Sample Output:
```
$ ./find-smudges G-ex_P100_50deg_exp100_2017-05-18_15h25m02s936ms-1.tif 
Found 222 smudges in image G-ex_P100_50deg_exp100_2017-05-18_15h25m02s936ms-1.tif[0]
398,7 6x6
192,9 6x6
28,14 6x6
272,15 6x6
395,16 6x6
48,18 6x6
22,19 6x6
31,19 6x6
... etc.

```

The [0] after the filename indicates the number of the image (TIFF files can contain more than one image).
There then follows a list of the bounding rectangles for the found smudges (col, row width x height).

### Viewing the image data
If you want to see the image data for each bounding rect, you can do so by changing `main.cpp`:
```
	cout << "Found " << rects.size() << " smudges in image " << im.name() << endl;
	for (auto& r : rects) {
		cout << r << endl;
		im.dump(cout, r);      // UNCOMMENT THIS LINE TO SEE IMAGE DATA FOR RECT
	}
```
The output then looks like this:
```
392, 127 6x6
 4  4  5  5  4  4 
 4  5  6  6  5  4 
 5  7  9  9  7  5 
 5  7  9  9  7  5 
 4  6  7  7  5  4 
 4  4  4  5  4  4 
361, 145 6x6
 5  5  5  5  5  5 
 5  5  6  6  5  5 
 5  6  8  8  6  5 
 5  6  8  8  6  5 
 5  6  6  6  6  4 
 5  5  5  5  5  4 
... etc.
```

## Algorithm

The algorithm is described in the comments in `SmudgeFinderWithSquareKernel.h`.

I looked into implementing a noise filter to process the image before running the smudge-finding algorithm, 
but I decided that it required more information about the nature of the noise. 
The algorithm itself is quite forgiving of noisy input.

The algorithm can be tuned via `MIN_SMUDGE_HEIGHT` in `main.cpp`:
```
	const int MIN_SMUDGE_HEIGHT = 2;
	SmudgeFinder::SmudgeFinderWithSquareKernel<6> smudgeFinder(im, MIN_SMUDGE_HEIGHT);
	auto rects = smudgeFinder.findSmudges();
```
Perhaps the setting of `2` for this parameter is too low? I chose the value 2 because the smudges returned
looked plausible in terms of their structure, although they would be very difficult (for me) to see with the naked eye on the images provided in the Coding Challenge,
and the specification stated that the smudges were "easy to spot with the naked eye".
But then I find it difficult to spot much with the naked eye in the images provided!

So, if it turns out that the program is finding far too many smudges, please tryincreasing MIN_SMUDGE_HEIGHT.
