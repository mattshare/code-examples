#include <iostream>
#include <stdexcept>
#include <tiffio.h> // for libtiff: http://www.libtiff.org/
#include "Rect.h"
#include "Image.h"
#include "SmudgeFinderWithSquareKernel.h"
using namespace std;

void processImage(const SmudgeFinder::Image& im) {

	const int MIN_SMUDGE_HEIGHT = 2;	// Higher values find fewer smudges.
	const int SQUARE_SIDE = 6;			// Implementation exists only for 6!

	SmudgeFinder::SmudgeFinderWithSquareKernel<SQUARE_SIDE> smudgeFinder(im, MIN_SMUDGE_HEIGHT);
	auto rects = smudgeFinder.findSmudges();
	
	cout << "Found " << rects.size() << " smudges in image " << im.name() << endl;
	for (auto& r : rects) {
		cout << r << endl;
		// im.dump(cout, r);
	}
}
int main(int argc, char*argv[]) {
	
	try {
		for (int i = 1; i < argc; ++i) {
			// All args are expected to be filenames of TIFF files:
			const char* filename = argv[i];
			TIFF* tif = TIFFOpen(filename, "r");
			if (tif) {
				int dircount = 0;
				// There may be more than one image in the TIFF file:
				do {
					const std::string name = filename + "["s + std::to_string(dircount) + "]"s;
					SmudgeFinder::Image im(name, tif);
					processImage(im);
					dircount++;
				} while (TIFFReadDirectory(tif));

				TIFFClose(tif);
			}
			else {
				throw std::runtime_error("TIFFOpen failed.");
			}
		}
	}
	catch(const std::exception& e) {
		cerr << "Exception caught: " << e.what() << endl;
	}
	catch(...) {
		cerr << "Unexpected exception caught: " << endl;
	}
}
