#ifndef RECT_H
#define RECT_H
#include <iostream>

namespace SmudgeFinder {
	struct Rect {
		Rect(int col, int row, int width, int height) :
		   col(col), row(row), width(width), height(height) {
		   }	   
		const int col;
		const int row;
		const int width;
		const int height;
	};
	std::ostream& operator<<(std::ostream& s, const Rect& r);

} // namespace SmudgeFinder

#endif // RECT_H
