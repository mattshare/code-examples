#include <algorithm>
#include <cassert>
#include <iomanip>	// for hex, setw, ...
#include <stdexcept>
#include <limits>   // for numeric_limits
#include <tiffio.h> // for libtiff: http://www.libtiff.org/
#include "Image.h"
#include "TestImage.h"

namespace SmudgeFinder {
	using namespace std;

	Image::Image(
			const std::string& name,
			TIFF* tif
			) : name_(name) {

		// Code based on:
		//    http://www.libtiff.org/libtiff.html#TIFFRGBAImage
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width_);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height_);
		const int npixels = width_*height_;
		uint32* raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
		if (raster != nullptr) {
			if (TIFFReadRGBAImage(tif, width_, height_, raster, 0)) {

				// We are dealing with  a greyscale image.
				// When unpacked into 32bit AGBR by TIFFReadRGBAImage, G, B and R are all set to
				// the same value. We arbitrarily grab the R velue below (with TIFFGetR). 
				// If the original image is 16bit greyscale, then precision will be lost
				// with just 8bits remaining.
				// However, this should be adequate for this application, where the data is known
				// to be noisy, and in any case, the precision of 16bits is not needed.
				//
				// We create a copy of the 8bit greyscale value of each pixel in the pixels_ vector.
				// We do this for two reasons:
				// 1. Performance: Call TIFFGetR just once per pixel. 
				//    This may speed things up as each pixel can be accessed dozens of times 
				//    as the algorithm executes.
				// 2. Robustness: Resource management of vector is more robust than that of 
				//    the plain pointers, malloc and free provided by libtiff.

				transform(raster, raster + npixels, std::back_inserter(pixels_),
						[](uint32 p) { return TIFFGetR(p); });
			}
			else
			{
				_TIFFfree(raster);
				throw std::runtime_error("TIFFReadRGBAImage failed.");
			}
		}
		else
		{
			throw std::bad_alloc();
		}
		_TIFFfree(raster);

		assert(Test::unitTest(*this));
	}
	vector<int> Image::histogram() const {
		// Returns a vector in which the ith element's value is equal to the number
		// of pixels in the image whose value is i.

		// How many different pixel values are possible? N:
		constexpr int N = numeric_limits<Pixel_t>::max() - numeric_limits<Pixel_t>::min() + 1;

		// Just in case someone changes Pixel_t to be something other than uint_8,
		// we need to think twice before allocating a massive vector!
		static_assert(N == 256, "Consider changing implementation of Image::histogram()");

		// The follow simply indexed vector depends on:
		static_assert(numeric_limits<Pixel_t>::min() == 0, "Implementation of Image::histogram() is wrong");

		vector<int> h(N, 0);
		for (Pixel_t i : pixels_)
			++h[i];
		return h;
	}
	void Image::dump(std::ostream& s) const {
		dump(s, Rect(0, 0, width_, height_));
	}
	void Image::dump(std::ostream& s, const Rect& rect) const {
		// save... stream state (so that our use of hex, for example
		// does not stay with the ostream after we return):
		std::ios_base::fmtflags f( s.flags() );

		for (int r = 0; r < rect.height; ++r) {
			for (int c = 0; c < rect.width; ++c) {
				s << hex << setw(2) << static_cast<int>(pixel(rect.col + c, rect.row + r)) << " ";
			}
			s << endl;
		}
		// restore...
		s.flags( f );
	}
} // namespace SmudgeFinder

