#include <iostream>
#include <cassert>
#include "Image.h"

// Basic unit testing of the SmudgeFinder::Image class.

namespace SmudgeFinder {
	namespace Test {

		using namespace std;

		// Unit test some member functions of SmudgeFinder::Image:
		bool unitTest(const SmudgeFinder::Image& image) {
			const int dim = 6;
			const int col = image.width()/2;
			const int row = image.height()/2;

			// Check that we've chosen valid row and col: 
			assert(row + dim <= image.height());
			assert(col + dim <= image.width());

			int i = 0;
			// Check that Image::subRow and Image::pixel give consistent results
			for (auto v : image.subRow(col, row, dim)) {
				int p = image.pixel(col + i, row);
				if (p != v) {
					image.dump(cout, SmudgeFinder::Rect(col, row, dim, dim));
					cout << "col: " << col <<
						",row: " << row <<
						", i: " << i <<
						", pixel: " << p <<
						", v: " << (int)v
						<< endl;
				}
				assert(p == v);
				++i;
			}
			assert(i == dim);

			// Check that Image::subColumn and Image::pixel give consistent results
			i = 0;
			for (auto v : image.subColumn(col, row, dim)) {
				int p = image.pixel(col, row + i);
				if (p != v) {
					image.dump(cout, SmudgeFinder::Rect(col, row, dim, dim));
					cout << "col: " << col <<
						",row: " << row <<
						", i: " << i <<
						", pixel: " << p <<
						", v: " << (int)v
						<< endl;
				}
				assert(p == v);
				++i;
			}
			assert(i == dim);
			return true;
		}
	} // namespace Text
} // namespace SmudgeFinder
