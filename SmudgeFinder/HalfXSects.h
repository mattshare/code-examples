#ifndef HALFXSECTS_H
#define HALFXSECTS_H
#include <vector>
#include <cassert>


namespace SmudgeFinder {

	template<unsigned int INDEX, unsigned int RAD>
		class HalfXSects {

			// HalfXSects is short for Half Cross Sections. See README.md for background.
			//
			//  RAD is the 'radius' of the square (half the width or height).
			//        RAD is also the length of each Half Cross Section.
			//
			//  INDEX indicates the rows and columns of interest: it specifies how far
			//        the rows/columns are from the edge of the square.
			//        e.g. with  RAD ==3, and INDEX of 2 spcifies the 2 central rows and columns.
			
			// There are 2 rows and 2 columns. Each has 2 half cross sections, making 8 in all:
			static const int COUNT = 8;

			// Generalizing this class would not be very difficult (using template specialization on the tamplate parameter RAD),
			// but it has not been done here because:
			//  1. The effort is not warranted
			//  2. The code is intended to be clearer as it deals only with the concrete 6x6 square
			static_assert(RAD == 3, "Implementation expects a RADius of 3. Otherwise create new template specialization.");
			static_assert(INDEX < RAD);
			const Image& image_;	// The image in which this square is contained
			const int row_;			// row index of the start of the square
			const int col_;			// col index of the start of the square
		public: 
			HalfXSects(const Image& image, int row, int col)
				: image_(image), row_(row), col_(col)	{ }
			class iterator {
				// The main purpose of the HalfXSects class is to provide an iterator
				// that steps through all COUNT of the half sections.
				// Algorithms that use this iterator will often not need to examine all
				// COUNT half sections, but will just look for the first hald section that does
				// not satisfy some predicate.
				// It is therefore important that we don't waste effort in creating half section objects
				// that are not needed. So all objects are created on-the-fly by operator*().
				
				// I is index of rows/cols that are a distance of INDEX from top/left of square:
				static constexpr int I = INDEX;

				// J is index of rows/cols that are a distance of INDEX from bottom/right of square:
				static constexpr int J = 2*RAD - INDEX - 1;

				// So, for a 6x6 square, we have:
				static_assert(!(I == 0) || J == 5); // indices for 'edge' rows/cols
				static_assert(!(I == 1) || J == 4);	// indices for 'in between'rows/cols
				static_assert(!(I == 2) || J == 3);	// indices for 'central' rows/cols

				// The HalfXSects class has INDEX as a template parameter in order to 
				// get the compiler to do some of the arithmetic.
				// The OFFSETS array also provides a simple way to manage the iterator's
				// progress - just keep track of how far the iterator is through the array.
				static constexpr int OFFSETS[HalfXSects::COUNT][RAD][2] = {
					// The offsets are the offsets from row_ and col_ that defines the position 
					// of the square in the image.

					// For the row  that is a distance INDEX from the top of the square, we have 2 half-rows:
					{{I, 0}, {I, 1}, {I, 2}},	// Offsets for first 3 pixels in row
					{{I, 5}, {I, 4}, {I, 3}},	// Offsets for last 3 pixels (reversed) in row

					// For the row  that is a distance INDEX from the bottom of the square, we have 2 half-rows:
					{{J, 0}, {J, 1}, {J, 2}},
					{{J, 5}, {J, 4}, {J, 3}},

					// For the column  that is a distance INDEX from the left of the square, we have 2 half-rows:
					{{0, I}, {1, I}, {2, I}},
					{{5, I}, {4, I}, {3, I}},

					// For the column  that is a distance INDEX from the right of the square, we have 2 half-rows:
					{{0, J}, {1, J}, {2, J}},
					{{5, J}, {4, J}, {3, J}}
				};

				// What is the 'container' through which we are iterating:
				const HalfXSects<INDEX, RAD>& cont_;

				// To keep track of how far the iterator is through the 8 half cross sections,
				// we maintain a pointer to how far we arethrough the offsets array:
				const int (*offset_)[RAD][2];
			public:
				explicit iterator(const HalfXSects<INDEX, RAD>& cont, int offset)
					: cont_(cont), offset_(&OFFSETS[offset]) { }
				typedef std::vector<int> value_type;
				typedef std::ptrdiff_t difference_type;
				typedef value_type* pointer;
				typedef value_type& reference;
				typedef std::input_iterator_tag iterator_category;

				value_type operator*() const {
					// There are opportunities for further optimization here:
					//   - if we know at compile time the WIDTH of the image, then by making
					//     WIDTH a template parameter, we can 
					//     pre-calculate offsets into image data further, rather than 
					//     have that done at run-time by the Image::pixel function.
					//   - perhaps by returning C++20 Ranges (rather than vectors)?
					//     These ranges could provide access directly to image pixel data
					//     rather than making a copy in a vector.
					//     For pixels in the same row, the 'stride' would be 1, and 
					//     for pixels in the same column, the stride would be WIDTH (of image).
					//   - perhaps by storing image data in a std::valarray, and then using std::slice,
					//     we may see performance improvement.
					// These require further investigation!
					return {
						cont_.image_.pixel(cont_.row_ + (*offset_)[0][0], cont_.col_ + (*offset_)[0][1]),
						cont_.image_.pixel(cont_.row_ + (*offset_)[1][0], cont_.col_ + (*offset_)[1][1]),
						cont_.image_.pixel(cont_.row_ + (*offset_)[2][0], cont_.col_ + (*offset_)[2][1])
					};
				}
				bool operator==(const iterator& other) const {
					// The test below on offset_ is necessary, but not sufficient to test equality.
					// For example, 
					//
					//   HalfXSect<1,3> a(im, 4, 3); // square starting at row 4, col 3
					//   HalfXSect<1,3> b(im, 5, 2); // square starting at row 5, col 2
					//   begin(a) == begin(b);       // returns true!!!
					//
					// To do the test 'properly', we sacrafice efficiency
					// (operator== may be called huge numbers of times).
					//
					// The 'proper' test is only of use to catch gross coding errors,
					// so we catch these in DEBUG mode:

					assert(cont_.row_ == other.cont_.row_);
					assert(cont_.col_ == other.cont_.col_);

					// Note: offset_ is pointer, but pointer to static member, 
					// so this should be fine!
					return offset_ == other.offset_;

				}
				bool operator!=(const iterator& other) const { return !(*this == other); }
				iterator& operator++()	// prefix ++
				{
					++offset_;
					return *this;
				}
				value_type operator++(int)	// postfix++ (best avoided!)
				{
					value_type ret = this->operator*();
					++*this;
					return ret;
				}
			};
			iterator begin() { return iterator(*this, 0); }
			iterator end() { return iterator(*this, COUNT); }
		};
} // namespace SmudgeFinder

#endif // HALFXSECTS_H
