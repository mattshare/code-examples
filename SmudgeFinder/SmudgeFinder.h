#ifndef SMUDGEFINDER_H
#define SMUDGEFINDER_H
#include <vector>
#include "Image.h"
#include "Rect.h"

namespace SmudgeFinder {
	// base class for all smudgefinder implementations
	// A SmudgeFinder takes an Image, and provides a list of bounding rects of the found smudges.
	//
	// We make this an abstract class to facilitate experimentation with different implementations.
	class SmudgeFinder {
	public:
		SmudgeFinder(const Image& image): image_(image) { };
		virtual std::vector<Rect> findSmudges() = 0;
	protected:
		const Image& image_;
	};
} // namespace SmudgeFinder
#endif // SMUDGEFINDER_H
