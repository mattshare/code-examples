#include "Rect.h"

namespace SmudgeFinder {
	std::ostream& operator<<(std::ostream& s, const Rect& r) {
		s << r.col << "," << r.row << " " << r.width << "x" << r.height;
		return s;
	}
} // namespace SmudgeFinder
