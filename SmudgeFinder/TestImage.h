#ifndef TESTIMAGE_H
#define TESTIMAGE_H
namespace SmudgeFinder {
	class Image;
	namespace Test {
		bool unitTest(const SmudgeFinder::Image& image);
	}
}
#endif // TESTIMAGE_H

