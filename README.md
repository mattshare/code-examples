# Coding Examples

Samples of code written by Matt Wallis.

## C++: image processing (2019)
A program for finding features in noisy images from a microscope.

Link to code: [Smudge Finder](SmudgeFinder);
The algorithm is descibed in [SmudgeFindefrWithSquareKernel.h](SmudgeFinder/SmudgeFinderWithSquareKernel.h).

## Ruby, Make: Generate and deploy Linked Data (2016 - 2019)
Ruby code and makefiles for generating Linked Data (rdf/xml and turtle) and HTML from CSV files, 
deploying the Linked Data to a server,
and generating Apache rewrite rules to serve data using the appropriate Linked Data URIs.

Link to code: [se-open-data on GitHub](https://github.com/SolidarityEconomyAssociation/open-data-and-maps/tree/master/data/tools/se_open_data)

An example dataset generated using this code:
* https://w3id.org/sea-lod/oxford/ If you visit this URI with a browser, you'll see the generated HTML. The HTML documents the generated Linked Data.

## Javascript: Geographic map powered by Linked Data (2016 - 2019)
Javascript application for displaying Linked Data resources on a [Leaflet](https://leafletjs.com/) map. 
The application uses the Model, View, Presenter (MVP) pattern.

Link to code: [map-app on GitHub](https://github.com/SolidarityEconomyAssociation/open-data-and-maps/tree/master/map-app/www/app)

Instances of the map can be seen running here: 
* https://oxford.solidarityeconomy.coop/
* https://forsolidarityne.solidarityeconomy.coop/

## Java: Railway ticket front-end (split-ticketing) (2014)
Java for a front-end for buying UK railway tickets, exploiting split-ticketing.
The Google Web Toolkit (GWT) was used to convert the java into javascript to run in the browser.

The java code here is mostly just 'boilerplate' to drive the UI using the GWT framework (not especially interesting!).

Line to code: [trainfair-frontend](trainfair-frontend/src/com/bonsaimangrove/ui/trainfair)
